package expectfile

import (
	"io/ioutil"
	"log"
	"time"

	yaml "gopkg.in/yaml.v3"
)

func NewExpectConfig(filename string) (*ExpectConfig, error) {
	ef := &ExpectConfig{}
	err := ef.Load(filename)
	if err != nil {
		return nil, err
	}

	return ef, nil
}

// TODO: agregar validaciones
func (f *ExpectConfig) Load(filename string) error {
	log.Printf("Cargando Definiciones desde %s", filename)
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal([]byte(content), &f)
	if err != nil {
		return err
	}
	f.parseScript(f.Script)
	f.Timeout.Global, err = time.ParseDuration(f.Timeout.GlobalStr)
	if err != nil {
		return err
	}
	f.Timeout.Command, err = time.ParseDuration(f.Timeout.CommandStr)
	if err != nil {
		return err
	}

	return nil
}

func (f *ExpectConfig) parseScript(script []map[string]interface{}) error {
	// for _, s := range script {
	// 	for k, v := range s {
	// 		switch k {
	// 		case "expect":
	// 			log.Printf("  %v -> %v\n", k, v)
	// 		case "send":
	// 			log.Printf("  %v -> %v\n", k, v)
	// 		case "sleep":
	// 			log.Printf("  %v -> %v\n", k, v)
	// 		}
	// 	}
	// }
	return nil
}
