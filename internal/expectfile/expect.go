package expectfile

import (
	"bytes"
	"fmt"
	"regexp"
	"text/template"

	"github.com/google/goexpect"
	"google.golang.org/grpc/codes"
)

func (f *ExpectConfig) ParseCommand(comand map[string]any, data map[string]string) (expect.Batcher, error) {
	var b bytes.Buffer
	for k, v := range comand {
		switch k {
		case "expect":
			ut, err := template.New("cmd").Parse(v.(string))
			if err != nil {
				return nil, fmt.Errorf("error al parsear el valor '%s' para el parametro '%s'", v, k)
			}
			ut.Execute(&b, data)
			return &expect.BExp{R: b.String()}, nil
		case "send":
			ut, err := template.New("cmd").Parse(v.(string))
			if err != nil {
				return nil, fmt.Errorf("error al parsear el valor '%s' para el parametro '%s'", v, k)
			}
			ut.Execute(&b, data)
			return &expect.BSnd{S: b.String()}, nil
		case "switch":
			bcase := []expect.Caser{}
			for _, item := range v.([]interface{}) {
				bc := item.(map[string]interface{})
				toSend := bc["send"].(string)
				toExpect := bc["case"].(string)
				tag := bc["tag"]
				toTag := ""
				if tag != nil {
					toTag = tag.(string)
				}
				switch toTag {
				case "fail", "Fail", "FAIL":
					bcase = append(bcase,
						&expect.Case{
							R: regexp.MustCompile(toExpect),
							S: toSend,
							T: expect.Fail(expect.NewStatus(codes.Aborted, "Fail")),
						})
				default:
					bcase = append(bcase, &expect.Case{R: regexp.MustCompile(toExpect), S: toSend, T: expect.OK()})
				}

			}
			return &expect.BCas{C: bcase}, nil
		default:
			return nil, fmt.Errorf("comando '%s' no reconocido", k)
		}
	}

	return nil, nil
}
