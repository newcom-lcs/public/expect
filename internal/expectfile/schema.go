package expectfile

import "time"

type ExpectConfig struct {
	Connection Connection               `json:"connection" yaml:"connection"`
	Timeout    Timeout                  `json:"timeout" yaml:"timeout"`
	Data       []map[string]string      `json:"data" yaml:"data"`
	Script     []map[string]interface{} `json:"script" yaml:"script"`
}

type Connection struct {
	Type         string `yaml:"type"`
	Address      string `yaml:"address"`
	Username     string `yaml:"username"`
	Password     string `yaml:"password"`
	PasswordFile string `yaml:"password_file"`
}

type Timeout struct {
	GlobalStr  string        `yaml:"global"`
	CommandStr string        `yaml:"command"`
	Global     time.Duration `yaml:"-"`
	Command    time.Duration `yaml:"-"`
	Action     string        `json:"action" yaml:"action"`
}
