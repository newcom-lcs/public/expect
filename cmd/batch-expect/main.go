package main

import (
	"flag"
	"fmt"
	"log"
	"time"

	// "regexp"
	// "time"

	"golang.org/x/crypto/ssh"

	// "google.golang.org/grpc/codes"

	"github.com/google/goexpect"
	"github.com/google/goterm/term"

	"gitlab.com/newcom-lcs/public/expect/internal/expectfile"
)

var (
	file = flag.String("f", "", "expect-file name")
)

func main() {
	flag.Parse()
	expectFile, err := expectfile.NewExpectConfig(*file)
	if err != nil {
		log.Fatalf("error al cargar archivo: %s", err)
	}

	sshClt, err := ssh.Dial("tcp", expectFile.Connection.Address, &ssh.ClientConfig{
		User:            expectFile.Connection.Username,
		Auth:            []ssh.AuthMethod{ssh.Password(expectFile.Connection.Password)},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	})
	if err != nil {
		log.Fatalf("ssh.Dial(%q) failed: %v", expectFile.Connection.Address, err)
	}
	defer sshClt.Close()

	fmt.Println(term.Bluef("Conectado"))

	e, _, err := expect.SpawnSSH(sshClt, expectFile.Timeout.Global)
	if err != nil {
		log.Fatal(err)
	}
	defer e.Close()

	timeout := expectFile.Timeout.Command
	for _, item := range expectFile.Data {
		batchList := []expect.Batcher{}
		time.Sleep(1 * time.Second)
		fmt.Printf("\nProcesando: ")
		for k, v := range item {
			fmt.Printf("\t%s: %s", k, v)
		}
		fmt.Printf(" ")
		for _, scriptLine := range expectFile.Script {
			batcher, err := expectFile.ParseCommand(scriptLine, item)
			if err != nil {
				log.Printf("Error al parear comando: %s", err)
			}
			if batcher != nil {
				batchList = append(batchList, batcher)
			}
		}
		res, err := e.ExpectBatch(batchList, timeout)
		if err != nil {
			if expectFile.Timeout.Action == "next" {
				// log.Println(err)
				fmt.Printf(" - %s", err)
			} else {
				log.Fatal(err)
			}
		} else {
			fmt.Printf(" - Ok")
		}
		for _, r := range res {
			fmt.Printf("%v", r.Output)
		}

	}

	fmt.Println(term.Greenf("\nCompletado!"))
}

// ssh-test:~#
