module gitlab.com/newcom-lcs/public/expect

go 1.18

require (
	github.com/google/goexpect v0.0.0-20210430020637-ab937bf7fd6f
	github.com/google/goterm v0.0.0-20200907032337-555d40f16ae2
	golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/sys v0.0.0-20210927094055-39ccf1dd6fa6 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/grpc v1.48.0 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
