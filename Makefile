build:
	go mod vendor
	go build ./cmd/batch-expect/
install:
	go mod vendor
	go install ./cmd/batch-expect/
dev:
	reflex -s -r '\.go$$' -- sh -c "go install ./cmd/batch-expect/"